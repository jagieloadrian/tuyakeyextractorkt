package com.anjo

import com.anjo.service.MenuService

fun main() {
    val menu = MenuService()
    menu.initializeMenu()
}