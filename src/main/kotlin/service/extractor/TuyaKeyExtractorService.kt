package com.anjo.service.extractor

import com.anjo.utils.ConsoleTableFormatter
import com.anjo.utils.Constants.DEFAULT_CSV_NAME
import com.anjo.utils.Constants.DEFAULT_TXT_NAME
import com.anjo.utils.Constants.INVALID_SEARCH_MESSAGE
import com.anjo.utils.writeFile
import java.io.FileOutputStream
import java.io.IOException

class TuyaKeyExtractorService {
    private val tuyaKeyXmlExtractor = TuyaKeyXmlExtractor()
    private val tuyaKeyJsonExtractor = TuyaKeyJsonExtractor()

    fun readRawJson(path: String) {
        val keys = tuyaKeyJsonExtractor.readKeys(path)
        keys.forEach {
            println(it)
        }
    }

    fun readRawXml(path: String) {
        val keys = tuyaKeyXmlExtractor.readKeys(path)
        keys.forEach {
            println(it)
        }
    }

    fun readJsonWithFormattedTable(path: String) {
        val keys = tuyaKeyJsonExtractor.readKeys(path)
        ConsoleTableFormatter.printFormattedTable(keys)
    }

    fun readXmlWithFormattedTable(path:String) {
        val keys = tuyaKeyXmlExtractor.readKeys(path)
        ConsoleTableFormatter.printFormattedTable(keys)
    }

    fun searchByDeviceId(path: String, searchQuery: String) {
        return if (searchQuery.isBlank()) {
            println(INVALID_SEARCH_MESSAGE)
        } else {
            tuyaKeyJsonExtractor.readKeys(path).firstOrNull { extractedDevice -> extractedDevice.deviceId == searchQuery }
                    ?.let { device -> println(device) }
                ?: println("Device with id $searchQuery not found in file with path: $path")
        }
    }

    fun saveToCsv(path: String) {
        try {
            val keys = tuyaKeyJsonExtractor.readKeys(path)
            FileOutputStream(DEFAULT_CSV_NAME).apply { writeFile(keys) }
        } catch (exc:IOException) {
            println(exc.message)
        }
    }

    fun saveToTxt(path: String) {
        try {
            val keys = tuyaKeyJsonExtractor.readKeys(path)
            FileOutputStream(DEFAULT_TXT_NAME).apply { writeFile(keys) }
        } catch (exc:IOException) {
            println(exc.message)
        }
    }
}