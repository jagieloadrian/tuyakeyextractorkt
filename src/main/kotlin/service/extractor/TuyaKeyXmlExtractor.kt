package com.anjo.service.extractor

import com.anjo.model.ExtractedDevice
import com.anjo.model.ExtractedDeviceParameter
import com.anjo.model.ExtractedDeviceParameter.DEV_ID
import com.anjo.model.ExtractedDeviceParameter.ICON_URL
import com.anjo.model.ExtractedDeviceParameter.LOCAL_KEY
import com.anjo.model.ExtractedDeviceParameter.NAME
import com.anjo.utils.Constants.AMPERSAD
import com.anjo.utils.Constants.COLON
import com.anjo.utils.Constants.DELIMITER_CHARS
import com.anjo.utils.Constants.EMPTY
import com.anjo.utils.validate
import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Files

class TuyaKeyXmlExtractor {

    fun readKeys(path: String): List<ExtractedDevice> {
        return extractKeys(path)
    }

    private fun extractKeys(path: String): List<ExtractedDevice> {
        val keywords = extractKeywords(path)
        val keys = extractDevices(keywords)
        return keys.toList()
    }

    private fun extractKeywords(path: String): List<String> {
        val file = File(path)
        if (file.validate().not()) return emptyList()
        val fileText = Files.readString(file.toPath(), StandardCharsets.UTF_8)
        return fileText.split(*DELIMITER_CHARS)
                .filter { it != EMPTY }
                .filter { it != COLON }
    }

    private fun extractDevices(keywords: List<String>): MutableList<ExtractedDevice> {
        val keys = mutableListOf<ExtractedDevice>()
        var device = ExtractedDevice()
        keywords.forEachIndexed { index, word ->
            val localIndex = index + 1
            when (word) {
                LOCAL_KEY.stringValue -> addParameter(device, keywords, localIndex, LOCAL_KEY)
                DEV_ID.stringValue    -> addParameter(device, keywords, localIndex, DEV_ID)
                NAME.stringValue      -> addParameter(device, keywords, localIndex, NAME)
                ICON_URL.stringValue  -> addParameter(device, keywords, localIndex, ICON_URL)
            }
            if (device.localKey.isNotEmpty() && device.deviceName.isNotEmpty() && device.iconUrl.isNotEmpty()) {
                keys.add(device)
                ExtractedDevice().also { device = it }
            }
        }
        return keys
    }

    private fun addParameter(
            device: ExtractedDevice, keywords: List<String>, localIndex: Int,
            parameter: ExtractedDeviceParameter) {
        when (parameter) {
            LOCAL_KEY -> {
                val replace = keywords[localIndex].replace(AMPERSAD, "&")
                device.localKey = replace
            }

            DEV_ID    -> {
                val replace = keywords[localIndex]
                device.deviceId = replace
            }

            NAME      -> {
                val replace = keywords[localIndex]
                device.deviceName = replace
            }

            ICON_URL  -> {
                val replace = keywords[localIndex]
                device.iconUrl = replace
            }
        }
    }
}