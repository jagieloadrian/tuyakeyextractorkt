package com.anjo.service.extractor

import TuyaKeyModel
import com.anjo.model.ExtractedDevice
import com.anjo.model.TuyaDeviceModel
import com.anjo.utils.Constants.EMPTY_TUYA_KEY
import com.anjo.utils.Constants.HOME_DATA
import com.anjo.utils.Constants.mapper
import com.anjo.utils.mapToExtractedDevice
import com.anjo.utils.validate
import com.fasterxml.jackson.module.kotlin.readValue
import org.json.XML
import java.io.File

class TuyaKeyJsonExtractor {

    fun readKeys(path: String): List<ExtractedDevice> {
        return extractKeys(path)
    }

    private fun extractKeys(path: String): List<ExtractedDevice> {
        val tuyaModel = getTuyaModelFromXml(path)

        val devicesInfoString = tuyaModel.tuyaMap.string.find { it.name.contains(HOME_DATA) }?.content
        val devicesInfoBatch: TuyaDeviceModel = devicesInfoString?.let { mapper.readValue(it) } ?: return emptyList()

        val devices = devicesInfoBatch.deviceRespBeen
        return devices.map {
            it.mapToExtractedDevice()
        }
    }

    private fun getTuyaModelFromXml(path: String): TuyaKeyModel {
        val file = File(path)
        if (file.validate().not()) return EMPTY_TUYA_KEY
        val jsonObj = XML.toJSONObject(file.readText())
        return mapper.readValue(jsonObj.toString())
    }
}