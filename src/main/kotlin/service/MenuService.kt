package com.anjo.service

import com.anjo.service.extractor.TuyaKeyExtractorService
import com.anjo.utils.Constants.BACKSLASH
import com.anjo.utils.Constants.DEFAULT_CSV_NAME
import com.anjo.utils.Constants.DEFAULT_TXT_NAME
import com.anjo.utils.Constants.EMPTY
import com.anjo.utils.Constants.ENTER_DEVICE_ID_OPTION
import com.anjo.utils.Constants.ENTER_MENU_OPTION
import com.anjo.utils.Constants.ENTER_TO_CONTINUE
import com.anjo.utils.Constants.ERROR_PATH_SET
import com.anjo.utils.Constants.GENERATED_INFO
import com.anjo.utils.Constants.GITLAB_URL
import com.anjo.utils.Constants.HOW_TO_USE_URL
import com.anjo.utils.Constants.LOCAL_TUYA_URL
import com.anjo.utils.Constants.PATH_SET
import com.anjo.utils.Constants.SET_PATH_INFO
import com.anjo.utils.Constants.THANKS_INFO
import com.anjo.utils.Constants.TRY_AGAIN
import com.anjo.utils.Constants.scanner
import com.anjo.utils.MenuUtils.aboutTool
import com.anjo.utils.MenuUtils.clearConsole
import com.anjo.utils.MenuUtils.getCurrentPath
import com.anjo.utils.MenuUtils.showPathError
import java.awt.Desktop
import java.lang.Exception
import java.net.URI
import kotlin.system.exitProcess

class MenuService {
    private val menuPrintService = MenuPrintService()
    private val extractorService = TuyaKeyExtractorService()

    fun initializeMenu() {
        menuEntry(null, 1)
    }

    private fun menuEntry(menuText: String?, menuToDisplay: Int) {
        clearConsole()
        menuPrintService.displayMenu(menuToDisplay)
        if (menuText.isNullOrBlank()) {
            println(ENTER_MENU_OPTION)
        } else {
            println("\n $menuText  : ")
        }
        val input = scanner.nextLine()
        processInput(input)
    }

    private fun processInput(input: String?) {
        if (input != null) {
            val skipContinue = false
            println("Entered : $input")
            when (input.uppercase().trim()) {
                "A"  -> aboutTool()
                "E"  -> menuEntry(null, 2)
                "1"  -> {
                    setPathOption()
                }

                "2"  -> {
                    //all keys from xml
                    validatePathAndRunAction { extractorService.readRawXml(menuPrintService.pathString) }
                }

                "3"  -> {
                    //all keys form xml by mapping json
                    validatePathAndRunAction { extractorService.readRawJson(menuPrintService.pathString) }
                }

                "4"  -> {
                    //all keys fancy from xml
                    validatePathAndRunAction { extractorService.readXmlWithFormattedTable(menuPrintService.pathString) }
                }

                "5"  -> {
                    //all keys fancy from mapping json
                    validatePathAndRunAction {
                        extractorService.readJsonWithFormattedTable(menuPrintService.pathString)
                    }
                }

                "6"  -> {
                    //print key by id from json
                    validatePathAndRunAction {
                        println(ENTER_DEVICE_ID_OPTION)
                        val searchQuery = scanner.nextLine()
                        extractorService.searchByDeviceId(menuPrintService.pathString, searchQuery)
                    }
                }

                "7"  -> {
                    //generate csv from json
                    validatePathAndRunAction {
                        extractorService.saveToCsv(menuPrintService.pathString)
                        val currentPath = getCurrentPath()
                        println(GENERATED_INFO(DEFAULT_CSV_NAME, currentPath))
                    }
                }

                "8"  -> {
                    //generate txt from json
                    validatePathAndRunAction {
                        extractorService.saveToTxt(menuPrintService.pathString)
                        val currentPath = getCurrentPath()
                        println(GENERATED_INFO(DEFAULT_TXT_NAME, currentPath))
                    }
                }

                "Q"  -> {
                    println(THANKS_INFO)
                    exitProcess(0)
                }

                "G"  -> urlOpener(GITLAB_URL)
                "V"  -> urlOpener(HOW_TO_USE_URL)
                "H"  -> urlOpener(LOCAL_TUYA_URL)
                "M"  -> menuEntry("", 1)
                else -> {
                    clearConsole()
                    println()
                    menuEntry(TRY_AGAIN, 1)
                }
            }
            if (!skipContinue) {
                println(ENTER_TO_CONTINUE)
                scanner.nextLine()
            }
        }
        menuEntry(null, 1)
    }

    private fun validatePathAndRunAction(action: () -> Unit) {
        if (menuPrintService.isPathSet) {
            action()
        } else {
            showPathError()
        }
    }

    private fun setPathOption() {
        println(SET_PATH_INFO)
        val readIn = scanner.nextLine()
        val path = readIn.replace(BACKSLASH, EMPTY).replace("'", EMPTY)
        if (menuPrintService.setConfigFilePath(path)) {
            println(PATH_SET)
        } else {
            println(ERROR_PATH_SET)
        }
    }

    private fun urlOpener(url: String) {
        try {
            val desktop = Desktop.getDesktop()
            val webUrl = URI(url)
            desktop.browse(webUrl)
        } catch (e: Exception) {
            println(e.printStackTrace())
        }
    }
}