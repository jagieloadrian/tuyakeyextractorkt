package com.anjo.service

import com.anjo.utils.Constants.NOT_SET
import com.anjo.utils.OptionConstants.ABOUT_COMMAND
import com.anjo.utils.OptionConstants.EIGHTH_OPTION
import com.anjo.utils.OptionConstants.EMPTY_COMMAND
import com.anjo.utils.OptionConstants.EXTRAS_OPTION
import com.anjo.utils.OptionConstants.FIFTH_OPTION
import com.anjo.utils.OptionConstants.FOURTH_OPTION
import com.anjo.utils.OptionConstants.GITLAB_COMMAND
import com.anjo.utils.OptionConstants.HOW_TO_COMMAND
import com.anjo.utils.OptionConstants.LINKS_COMMAND
import com.anjo.utils.OptionConstants.MAIN_MENU_COMMAND
import com.anjo.utils.OptionConstants.PRESS_M_COMMAND
import com.anjo.utils.OptionConstants.QUIT_OPTION
import com.anjo.utils.OptionConstants.RELATED_VIDEOS_COMMAND
import com.anjo.utils.OptionConstants.SECOND_OPTION
import com.anjo.utils.OptionConstants.SET_CONFIG_PATH_OPTION
import com.anjo.utils.OptionConstants.SEVENTH_OPTION
import com.anjo.utils.OptionConstants.SIXTH_OPTION
import com.anjo.utils.OptionConstants.THIRD_OPTION
import com.anjo.utils.OptionConstants.VIDEO_COMMAND


class MenuPrintService {
    private val menuOptions = mutableListOf<String>()
    private val allCommands = mutableListOf<String>()

    init {
        buildMenu()
        buildAllCommands()
    }

    private var configFilePath: String = ""
    val pathString
        get() = if (isPathSet) configFilePath else NOT_SET
    val isPathSet
        get() = !configFilePath.isNullOrBlank()

    fun setConfigFilePath(path: String): Boolean {
        configFilePath = path
        return isPathSet
    }

    fun displayMenu(menu: Int) {
        when (menu) {
            1 -> printOnlyMenuOptions()
            2 -> printAllCommands()
        }
    }

    private fun printAllCommands() {
        allCommands.clear()
        buildAllCommands()
        displayHeading()
        for (command in allCommands) {
            println(command)
        }
    }

    private fun printOnlyMenuOptions() {
        menuOptions.clear()
        buildMenu()
        displayHeading()
        for (option in menuOptions) {
            println(option)
        }
    }

    private fun buildMenu() {
        menuOptions.add(SET_CONFIG_PATH_OPTION(pathString))
        menuOptions.add(SECOND_OPTION)
        menuOptions.add(THIRD_OPTION)
        menuOptions.add(FOURTH_OPTION)
        menuOptions.add(FIFTH_OPTION)
        menuOptions.add(SIXTH_OPTION)
        menuOptions.add(SEVENTH_OPTION)
        menuOptions.add(EIGHTH_OPTION)
        menuOptions.add(EXTRAS_OPTION)
        menuOptions.add(QUIT_OPTION)
    }

    private fun buildAllCommands() {
        for (option in menuOptions) {
            allCommands.add(option)
        }
        allCommands.add(EMPTY_COMMAND)
        allCommands.add(MAIN_MENU_COMMAND)
        allCommands.add(ABOUT_COMMAND)
        allCommands.add(EMPTY_COMMAND)
        allCommands.add(LINKS_COMMAND)
        allCommands.add(GITLAB_COMMAND)
        allCommands.add(EMPTY_COMMAND)
        allCommands.add(RELATED_VIDEOS_COMMAND)
        allCommands.add(VIDEO_COMMAND)
        allCommands.add(HOW_TO_COMMAND)
        allCommands.add(EMPTY_COMMAND)
        allCommands.add(PRESS_M_COMMAND)
    }

    private fun displayHeading() {
        println("===================================================================================\n")
        println("   ████████╗     ██╗   ████████╗    ")
        println("   ██║    ██║    ██║   ██║   ██║    ")
        println("   ██║     ██║   ██║   ██║   ██║    ")
        println("   ██║      ██║  ██║   ████████║    ")
        println("   ██║     ██║   ██║   ██║   ██║    ")
        println("   ██║    ██║    ██║   ██║   ██║    -  Tuya Key Extractor ")
        println("   ████████║     ██║   ████████║    -  influenced by Mark Watt")
        println("   ╚═══════╝     ╚═╝   ╚═══════╝  \n")
        println("===================================================================================\n")
    }
}