package com.anjo.model

enum class ExtractedDeviceParameter(val stringValue: String) {
    LOCAL_KEY("localKey"),
    DEV_ID("devId"),
    NAME("name"),
    ICON_URL("iconUrl")
}