import com.fasterxml.jackson.annotation.JsonProperty

data class TuyaKeyModel(
        @JsonProperty("map")
        val tuyaMap: TuyaMap
)

data class TuyaMap(
        @JsonProperty("boolean")
        val boolean: BooleanProperty,
        @JsonProperty("int")
        val int: IntProperty,
        @JsonProperty("string")
        val string: List<StringProperty>
)

data class BooleanProperty(
        @JsonProperty("name")
        val name: String,
        @JsonProperty("value")
        val value: Boolean
)

data class IntProperty(
        @JsonProperty("name")
        val name: String,
        @JsonProperty("value")
        val value: Int
)

data class StringProperty(
        @JsonProperty("content")
        val content: String,
        @JsonProperty("name")
        val name: String
)