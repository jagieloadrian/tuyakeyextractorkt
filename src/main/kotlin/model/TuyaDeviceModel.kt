package com.anjo.model

import com.fasterxml.jackson.annotation.JsonProperty

data class TuyaDeviceModel(
        @JsonProperty("deviceRespBeen")
        val deviceRespBeen: List<DeviceRespBeen>,
        @JsonProperty("deviceRespShareList")
        val deviceRespShareList: List<Any>,
        @JsonProperty("groupBeen")
        val groupBeen: List<Any>,
        @JsonProperty("groupRespShareList")
        val groupRespShareList: List<Any>,
        @JsonProperty("homeResponseBean")
        val homeResponseBean: HomeResponseBean,
        @JsonProperty("meshBeen")
        val meshBeen: List<Any>,
        @JsonProperty("productBeen")
        val productBeen: List<ProductBeen>,
        @JsonProperty("relation")
        val relation: String
)

data class DeviceRespBeen(
        @JsonProperty("activeTime")
        val activeTime: Int,
        @JsonProperty("devId")
        val devId: String,
        @JsonProperty("displayOrder")
        val displayOrder: Int,
        @JsonProperty("dpMaxTime")
        val dpMaxTime: Long,
        @JsonProperty("dpName")
        val dpName: DpName,
        @JsonProperty("dps")
        val dps: Map<Int,Any>,
        @JsonProperty("errorCode")
        val errorCode: Int,
        @JsonProperty("iconUrl")
        val iconUrl: String,
        @JsonProperty("isShare")
        val isShare: Boolean,
        @JsonProperty("key")
        val key: String,
        @JsonProperty("lat")
        val lat: String,
        @JsonProperty("localKey")
        val localKey: String,
        @JsonProperty("lon")
        val lon: String,
        @JsonProperty("moduleMap")
        val moduleMap: ModuleMap,
        @JsonProperty("name")
        val name: String,
        @JsonProperty("productId")
        val productId: String,
        @JsonProperty("resptime")
        val resptime: Int,
        @JsonProperty("runtimeEnv")
        val runtimeEnv: String,
        @JsonProperty("timezoneId")
        val timezoneId: String,
        @JsonProperty("uuid")
        val uuid: String,
        @JsonProperty("virtual")
        val virtual: Boolean
)

data class HomeResponseBean(
        @JsonProperty("admin")
        val admin: Boolean,
        @JsonProperty("background")
        val background: String,
        @JsonProperty("geoName")
        val geoName: String,
        @JsonProperty("gid")
        val gid: Int,
        @JsonProperty("id")
        val id: Int,
        @JsonProperty("lat")
        val lat: Int,
        @JsonProperty("lon")
        val lon: Int,
        @JsonProperty("name")
        val name: String,
        @JsonProperty("rooms")
        val rooms: List<Any>
)

data class ProductBeen(
        @JsonProperty("attribute")
        val attribute: Int,
        @JsonProperty("capability")
        val capability: Int,
        @JsonProperty("category")
        val category: String,
        @JsonProperty("i18nTime")
        val i18nTime: Int,
        @JsonProperty("id")
        val id: String,
        @JsonProperty("infraredSubDevDisplayInList")
        val infraredSubDevDisplayInList: Boolean,
        @JsonProperty("key")
        val key: String,
        @JsonProperty("meshCategory")
        val meshCategory: String,
        @JsonProperty("panelConfig")
        val panelConfig: PanelConfig,
        @JsonProperty("resptime")
        val resptime: Long,
        @JsonProperty("schemaInfo")
        val schemaInfo: SchemaInfo,
        @JsonProperty("shortcut")
        val shortcut: Shortcut,
        @JsonProperty("supportGroup")
        val supportGroup: Boolean,
        @JsonProperty("uiInfo")
        val uiInfo: UiInfo,
        @JsonProperty("wifi")
        val wifi: Boolean
)

class DpName

data class ModuleMap(
        @JsonProperty("bluetooth")
        val bluetooth: Bluetooth
)

data class Bluetooth(
        @JsonProperty("bv")
        val bv: String,
        @JsonProperty("cadv")
        val cadv: String,
        @JsonProperty("isOnline")
        val isOnline: Boolean,
        @JsonProperty("pv")
        val pv: String,
        @JsonProperty("verSw")
        val verSw: String
)

data class PanelConfig(
        @JsonProperty("bic")
        val bic: List<Bic>
)

data class SchemaInfo(
        @JsonProperty("schema")
        val schema: String,
        @JsonProperty("schemaExt")
        val schemaExt: String,
        @JsonProperty("schemaMap")
        val schemaMap: Map<Int,MapValue>
)

data class Shortcut(
        @JsonProperty("displayDps")
        val displayDps: List<Any>,
        @JsonProperty("displayMsgs")
        val displayMsgs: DisplayMsgs,
        @JsonProperty("faultDps")
        val faultDps: List<Any>,
        @JsonProperty("quickOpDps")
        val quickOpDps: List<Any>,
        @JsonProperty("switchDp")
        val switchDp: Int
)

data class UiInfo(
        @JsonProperty("appRnVersion")
        val appRnVersion: String,
        @JsonProperty("phase")
        val phase: String,
        @JsonProperty("rnFind")
        val rnFind: Boolean,
        @JsonProperty("type")
        val type: String,
        @JsonProperty("ui")
        val ui: String,
        @JsonProperty("uiConfig")
        val uiConfig: UiConfig
)

data class Bic(
        @JsonProperty("code")
        val code: String,
        @JsonProperty("selected")
        val selected: Boolean
)

data class MapValue(
        @JsonProperty("code")
        val code: String,
        @JsonProperty("iconname")
        val iconname: String,
        @JsonProperty("id")
        val id: String,
        @JsonProperty("mode")
        val mode: String,
        @JsonProperty("name")
        val name: String,
        @JsonProperty("property")
        val `property`: String,
        @JsonProperty("schemaType")
        val schemaType: String,
        @JsonProperty("type")
        val type: String
)

class DisplayMsgs

class UiConfig