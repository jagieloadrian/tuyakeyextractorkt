package com.anjo.model

data class ExtractedDevice(
        var deviceName: String = "",
        var localKey: String = "",
        var deviceId: String = "",
        var iconUrl: String = "",
)
