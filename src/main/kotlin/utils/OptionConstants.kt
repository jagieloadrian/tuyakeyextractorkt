package com.anjo.utils

object OptionConstants {
    fun SET_CONFIG_PATH_OPTION(pathString:String) = "  1   -   Set Config Filepath              |    [ PATH : $pathString ]"
    const val SECOND_OPTION = "  2   -   Print All Keys from xml          | "
    const val THIRD_OPTION = "  3   -   Print All Keys from json mapping | "
    const val FOURTH_OPTION = "  4   -   Print All Keys fancy from xml    | "
    const val FIFTH_OPTION = "  5   -   Print All Keys fancy from json   | "
    const val SIXTH_OPTION = "  6   -   Print Key by ID                  | "
    const val SEVENTH_OPTION = "  7   -   Generate .CSV                    | "
    const val EIGHTH_OPTION = "  8   -   Generate .TXT                    | "
    const val EXTRAS_OPTION = "  E   -   Extras                           | "
    const val QUIT_OPTION = "  Q   -   Quit                             | "
    const val EMPTY_COMMAND = "                                     | "
    const val MAIN_MENU_COMMAND = "  M   -   Main Menu                  | "
    const val ABOUT_COMMAND = "  A   -   About this tool            | "
    const val LINKS_COMMAND = "  Links for all My Socials           | "
    const val GITLAB_COMMAND = "  G   -   GitLab for this Tool       | "
    const val RELATED_VIDEOS_COMMAND = "  RELATED VIDEOS                     | "
    const val VIDEO_COMMAND = "  V   -   Video on Local Tuya        | "
    const val HOW_TO_COMMAND = "  H   -   How to use this tool       | "
    const val PRESS_M_COMMAND = "  Press 'M' to return to Basic Menu  | "
}