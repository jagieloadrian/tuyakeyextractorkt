package com.anjo.utils

import com.anjo.model.DeviceRespBeen
import com.anjo.model.ExtractedDevice
import com.anjo.utils.Constants.COLUMN_NAMES
import com.anjo.utils.Constants.DIFFERENT_FILE_MESSAGE
import com.anjo.utils.Constants.FILE_NOT_EXIST_MESSAGE
import com.anjo.utils.Constants.XML_EXTENSION
import java.io.File
import java.io.OutputStream

fun File.validate(): Boolean {
    if (this.exists().not()) {
        println(FILE_NOT_EXIST_MESSAGE)
        return false
    }
    if (this.extension != XML_EXTENSION) {
        println(DIFFERENT_FILE_MESSAGE)
        return false
    }
    return true
}

fun DeviceRespBeen.mapToExtractedDevice(): ExtractedDevice {
    return ExtractedDevice(
            deviceName = this.name,
            localKey = this.localKey,
            deviceId = this.devId,
            iconUrl = this.iconUrl
    )
}

fun OutputStream.writeFile(keys: List<ExtractedDevice>) {
    val writer = bufferedWriter()
    writer.write(COLUMN_NAMES.joinToString())
    writer.newLine()
    keys.forEach {
        writer.write("\"${it.deviceId}\", \"${it.deviceName}\", \"${it.localKey}\", \"${it.iconUrl}\"")
        writer.newLine()
    }
    writer.flush()
    writer.close()
}
