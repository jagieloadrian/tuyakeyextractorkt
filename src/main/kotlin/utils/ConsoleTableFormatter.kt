package com.anjo.utils

import com.anjo.model.ExtractedDevice
import com.anjo.utils.Constants.COLUMN_NAMES
import de.vandermeer.asciitable.AsciiTable
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment

object ConsoleTableFormatter {
    fun printFormattedTable(keys: List<ExtractedDevice>) {
        val asciiTable = AsciiTable()
        asciiTable.addRule()
        asciiTable.addRow(*COLUMN_NAMES)
        asciiTable.addRule()
        keys.forEach { key ->
            asciiTable.addRow(key.deviceId, key.deviceName, key.localKey, key.iconUrl)
            asciiTable.addRule()
        }
        asciiTable.setTextAlignment(TextAlignment.CENTER)
        val render = asciiTable.render()
        println(render)
    }
}