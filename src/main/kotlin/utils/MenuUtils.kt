package com.anjo.utils

import com.anjo.utils.Constants.ABOUT_TOOL
import com.anjo.utils.Constants.EMPTY
import com.anjo.utils.Constants.END_HORIZONTAL_LINE
import com.anjo.utils.Constants.ERROR_PATH_MESSAGE
import com.anjo.utils.Constants.LINUX_CLEAR_SCREEN_COMMAND
import com.anjo.utils.Constants.OS_NAME_PROPERTY
import com.anjo.utils.Constants.START_HORIZONTAL_LINE
import com.anjo.utils.Constants.WINDOWS_CLEAR_SCREEN_COMMANDS
import com.anjo.utils.Constants.WINDOWS_OS_NAME
import java.io.File

object MenuUtils {
    fun getCurrentPath(): String {
        return File(EMPTY).absolutePath
    }

    fun showPathError() {
        println(ERROR_PATH_MESSAGE)
    }

    fun aboutTool() {
        println(START_HORIZONTAL_LINE)
        println(ABOUT_TOOL)
        println(END_HORIZONTAL_LINE)
    }

    fun clearConsole() {
        try {
            val os = System.getProperty(OS_NAME_PROPERTY)
            if (os.contains(WINDOWS_OS_NAME)) {
                ProcessBuilder(WINDOWS_CLEAR_SCREEN_COMMANDS)
                        .inheritIO().start().waitFor()
            } else {
                ProcessBuilder(LINUX_CLEAR_SCREEN_COMMAND)
                        .inheritIO().start().waitFor()
            }
        } catch (e: Exception) {
           println(e.message)
        }
    }
}