package com.anjo.utils

import BooleanProperty
import IntProperty
import StringProperty
import TuyaKeyModel
import TuyaMap
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.Scanner

object Constants {

    val DELIMITER_CHARS = arrayOf(",", "&quot;")
    val COLUMN_NAMES = arrayOf("Device Id", "Device Name", "Local Key", "Icon Url")
    val mapper = ObjectMapper()
    val scanner = Scanner(System.`in`)
    const val DEFAULT_CSV_NAME = "extractedDevices.csv"
    const val DEFAULT_TXT_NAME = "extractedDevices.txt"
    const val AMPERSAD = "&amp;"
    const val HOME_DATA = "home_data"
    const val BACKSLASH = "\""
    const val EMPTY = ""
    const val COLON = ":"
    const val GITLAB_URL = "https://gitlab.com/jagieloadrian/tuyakeyextractorkt"
    const val HOW_TO_USE_URL = "https://youtu.be/F00_4jDk06g"
    const val LOCAL_TUYA_URL = "https://youtu.be/YKvGYXw-_cE"
    const val ERROR_PATH_MESSAGE = "ERROR - You need to specify the filepath for the preference.xml file first."
    const val START_HORIZONTAL_LINE = "\n==================================================================================================="
    const val END_HORIZONTAL_LINE = "===================================================================================================\n"
    const val ABOUT_TOOL = """I (diether18) influenced by Mark Watt re-write this tool as a simple way for me to read my local keys from my
                extracted SmartLife config. I decided to share this tool with others as it may help to simplify the process
                for others. This is just something I quickly put together to fit my need. If anybody wants to add and
                contribute to it, then please feel free to put in pull requests. Or if theres a small change you would
                like me to make then let me know and I can try and get it added! I may try and tidy it up and standardise it. Who knows :P
                The tool is designed to be used with Mark Weed Local Tuya Key Extraction video.
                I hope the original author won't be mad that I reuse his business logic to re-write service in kotlin to use also in Linux systems :)
                Cheers, d18"""
    const val OS_NAME_PROPERTY = "os.name"
    const val WINDOWS_OS_NAME = "Windows"
    val WINDOWS_CLEAR_SCREEN_COMMANDS = listOf("cmd", "/c", "cls")
    const val LINUX_CLEAR_SCREEN_COMMAND = "clear"
    const val XML_EXTENSION = "xml"
    const val DIFFERENT_FILE_MESSAGE = "This is a different file than xml! Please, check again file!"
    const val FILE_NOT_EXIST_MESSAGE = "File doesn't exist! Check again path!"
    const val INVALID_SEARCH_MESSAGE = "Invalid Search... Please try again."
    const val NOT_SET = "Not Set"
    const val SET_PATH_INFO = "Enter the file path for the Preference.xml file   (e.g. C:\\Users\\MarkWattTech\\Desktop\\Preference.xml or /home/preference.xml)"
    const val PATH_SET = "PATH has been set"
    const val ERROR_PATH_SET = "ERROR - Enter a valid file path"
    const val ENTER_TO_CONTINUE = "\n Press Enter to Continue."
    const val TRY_AGAIN = "Unknown, try again"
    const val ENTER_MENU_OPTION = "\n Enter a Menu Option : "
    const val ENTER_DEVICE_ID_OPTION = "Enter the Device ID you would like the local key for: "
    const val THANKS_INFO = "Thanks for checking out the tool!"
    fun GENERATED_INFO(defaultName:String, currentPath: String) = "Successfully generated $defaultName in folder: $currentPath"

    private val tuyaBoolean = BooleanProperty(EMPTY, false)
    private val tuyaInt = IntProperty(EMPTY, 0)
    private val tuyaString = listOf<StringProperty>()
    private val tuyaMap = TuyaMap(tuyaBoolean, tuyaInt, tuyaString)
    val EMPTY_TUYA_KEY = TuyaKeyModel(tuyaMap)
}