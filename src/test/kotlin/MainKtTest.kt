import com.anjo.main
import com.anjo.service.MenuService
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockkConstructor
import io.mockk.verify
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.test.Test

@ExtendWith(MockKExtension::class)
class MainKtTest {

    @Test
    fun `given menuService when call main then verify call`() {
        //given
        mockkConstructor(MenuService::class)
        every { anyConstructed<MenuService>().initializeMenu() } returns Unit

        //when
        main()

        //then
        verify { anyConstructed<MenuService>().initializeMenu() }
    }
}