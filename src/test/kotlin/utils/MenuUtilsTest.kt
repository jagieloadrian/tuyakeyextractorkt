package utils

import com.anjo.utils.Constants.ABOUT_TOOL
import com.anjo.utils.Constants.END_HORIZONTAL_LINE
import com.anjo.utils.Constants.ERROR_PATH_MESSAGE
import com.anjo.utils.Constants.START_HORIZONTAL_LINE
import com.anjo.utils.MenuUtils.aboutTool
import com.anjo.utils.MenuUtils.clearConsole
import com.anjo.utils.MenuUtils.getCurrentPath
import com.anjo.utils.MenuUtils.showPathError
import com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut
import constant.TestConstant.CURRENT_PATH
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.mockk.every
import io.mockk.mockkConstructor
import io.mockk.verify
import java.io.IOException
import kotlin.test.Test

class MenuUtilsTest {
    @Test
    fun `given absolute path when getCurrentPath then should be the same`() {
        //when
        val actual = getCurrentPath()

        //then
        actual shouldBe CURRENT_PATH
    }

    @Test
    fun `given expected message when showPathError then assert messages`() {
        //given
        val expected = ERROR_PATH_MESSAGE

        //when
        val actual = tapSystemOut {
            showPathError()
        }

        //then
        actual shouldContain expected
    }

    @Test
    fun `given expected messages when aboutTool then assert messages`() {
        //given
        val expected = "$START_HORIZONTAL_LINE\n$ABOUT_TOOL\n$END_HORIZONTAL_LINE"

        //when
        val actual = tapSystemOut {
            aboutTool()
        }

        //then
        actual shouldContain expected
    }

    @Test
    fun `given mock process command when clearConsole then return empty string and verify call`() {
        //given
        mockkConstructor(ProcessBuilder::class)
        every { anyConstructed<ProcessBuilder>().inheritIO().start().waitFor() } returns 1

        //when
        val actual = tapSystemOut {
            clearConsole()
        }

        //then
        actual shouldBe ""
        verify { anyConstructed<ProcessBuilder>().inheritIO().start().waitFor() }
    }

    @Test
    fun `given mock process command and throw exception when clearConsole then print stackTrace`() {
        //given
        mockkConstructor(ProcessBuilder::class)
        every { anyConstructed<ProcessBuilder>().inheritIO().start() } throws IOException(IOException().javaClass.simpleName)

        //when
        val actual = tapSystemOut {
            clearConsole()
        }

        //then
        actual shouldContain IOException().javaClass.simpleName
    }
}