package utils

import com.anjo.model.ExtractedDevice
import com.anjo.utils.ConsoleTableFormatter
import com.anjo.utils.Constants.COLUMN_NAMES
import com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut
import constant.TestConstant.DEVICE_ID_TEST
import constant.TestConstant.DEVICE_NAME_TEST
import constant.TestConstant.ICON_URL_TEST
import constant.TestConstant.LOCAL_KEY_TEST
import io.kotest.matchers.string.shouldContain
import org.junit.jupiter.api.Test

class ConsoleTableFormatterTest {

    @Test
    fun `given extractedDevice when printFormattedTable then assert is have properties`() {
        //given
        val extractedDevice = ExtractedDevice(DEVICE_NAME_TEST, LOCAL_KEY_TEST, DEVICE_ID_TEST, ICON_URL_TEST)

        val keys = listOf(extractedDevice)

        //when
        val actual = tapSystemOut {
            ConsoleTableFormatter.printFormattedTable(keys)
        }

        //then
        actual shouldContain  extractedDevice.deviceId
        actual shouldContain  extractedDevice.localKey
        actual shouldContain  extractedDevice.deviceName
        actual shouldContain  extractedDevice.iconUrl
        COLUMN_NAMES.forEach {
            actual shouldContain  it
        }
    }
}