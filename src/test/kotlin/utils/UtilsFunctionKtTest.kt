package utils

import com.anjo.model.Bluetooth
import com.anjo.model.DeviceRespBeen
import com.anjo.model.DpName
import com.anjo.model.ExtractedDevice
import com.anjo.model.ModuleMap
import com.anjo.utils.Constants.DIFFERENT_FILE_MESSAGE
import com.anjo.utils.Constants.FILE_NOT_EXIST_MESSAGE
import com.anjo.utils.Constants.XML_EXTENSION
import com.anjo.utils.mapToExtractedDevice
import com.anjo.utils.validate
import com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut
import constant.TestConstant.DEVICE_ID_TEST
import constant.TestConstant.DEVICE_NAME_TEST
import constant.TestConstant.ICON_URL_TEST
import constant.TestConstant.LOCAL_KEY_TEST
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.jupiter.api.Test
import java.io.File

class UtilsFunctionKtTest {

    @Test
    fun `given DeviceRaspBeen object when mapToExtractedDevice then return extractedDevice`() {
        //given
        val raspBeen = DeviceRespBeen(1, DEVICE_ID_TEST, 0, 0L, DpName(), mapOf(),
                0, ICON_URL_TEST, true, "key", "lat", LOCAL_KEY_TEST, "lon",
                ModuleMap(Bluetooth("bv", "cadv", true, "pv", "verSw")), DEVICE_NAME_TEST,
                "productId", 0, "env", "timezone", "uuid", false)
        val expected = ExtractedDevice(DEVICE_NAME_TEST, LOCAL_KEY_TEST, DEVICE_ID_TEST, ICON_URL_TEST)

        //when
        val actual = raspBeen.mapToExtractedDevice()

        //then
        actual shouldBe expected
    }

    @Test
    fun `given not exist file when validate then return message`() {
        //given
        val mockFile = mockk<File>()
        every { mockFile.exists() } returns false
        val expected = FILE_NOT_EXIST_MESSAGE

        //when
        val actual = tapSystemOut {
            mockFile.validate()
        }

        //then
        actual shouldContain expected
    }

    @Test
    fun `given file with different extension than xml when validate then return message`() {
        //given
        val mockFile = mockk<File>()
        every { mockFile.exists() } returns true
        every { mockFile.extension } returns "differentExt"
        val expected = DIFFERENT_FILE_MESSAGE

        //when
        val actual = tapSystemOut {
            mockFile.validate()
        }

        //then
        actual shouldContain expected
    }

    @Test
    fun `given file than xml when validate then return true`() {
        //given
        val mockFile = mockk<File>()
        mockkStatic("kotlin.io.FilesKt__UtilsKt")
        every { mockFile.exists() } returns true
        every { mockFile.extension } answers
                {XML_EXTENSION }
        val expected = true

        //when
        val actual = mockFile.validate()

        //then
        actual shouldBe expected
    }
}