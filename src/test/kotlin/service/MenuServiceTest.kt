package service

import com.anjo.service.MenuService
import com.anjo.utils.Constants.DEFAULT_CSV_NAME
import com.anjo.utils.Constants.DEFAULT_TXT_NAME
import com.github.stefanbirkner.systemlambda.SystemLambda.catchSystemExit
import com.github.stefanbirkner.systemlambda.SystemLambda.withTextFromSystemIn
import constant.TestConstant.CURRENT_PATH
import constant.TestConstant.TESTED_COMMAND_OPTIONS
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import io.mockk.verify
import org.junit.jupiter.api.Test
import java.awt.Desktop
import java.io.File

class MenuServiceTest {

    @Test
    fun `given tested class and arguments when initializeMenu then verify calls`() {
        //given
        val menuService = MenuService()
        val currentDesktop = mockk<Desktop>()
        mockkStatic(Desktop::class)
        every { Desktop.getDesktop() } returns currentDesktop
        every { currentDesktop.browse(any()) } returns Unit

        //when
        val actual = catchSystemExit {
            withTextFromSystemIn(*TESTED_COMMAND_OPTIONS)
                    .execute {
                        menuService.initializeMenu()
                    }
        }

        //then
        actual shouldBe 0
        verify(exactly = 3) { currentDesktop.browse(any()) }
        cleanFiles()
        unmockkAll()
    }

    private fun cleanFiles() {
        listOf(DEFAULT_TXT_NAME, DEFAULT_CSV_NAME).forEach { fileName ->
            val actual = File("$CURRENT_PATH/$fileName")
            actual.exists() shouldBe true
            actual.delete()
        }
    }
}