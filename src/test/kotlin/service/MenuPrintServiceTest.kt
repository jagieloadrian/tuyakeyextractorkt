package service

import com.anjo.service.MenuPrintService
import com.github.stefanbirkner.systemlambda.SystemLambda
import constant.TestConstant.ALL_COMMANDS_EXPECTED
import constant.TestConstant.MENU_OPTION_EXPECTED
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import org.junit.jupiter.api.Test

class MenuPrintServiceTest {

    @Test
    fun `given first option when displayMenu then return option in console menu`() {
        //given
        val menuPrintService = MenuPrintService()

        //when
        val actual = SystemLambda.tapSystemOut {
            menuPrintService.displayMenu(1)
        }

        //then
        MENU_OPTION_EXPECTED.forEach { option ->
            actual shouldContain option
        }
    }

    @Test
    fun `given second option when displayMenu then return all commands in console menu`() {
        //given
        val menuPrintService = MenuPrintService()

        //when
        val actual = SystemLambda.tapSystemOut {
            menuPrintService.displayMenu(2)
        }

        //then
        ALL_COMMANDS_EXPECTED.forEach { option ->
            actual shouldContain option
        }
    }

    @Test
    fun `given some string when setConfigFilePath then return true`() {
        //given
        val path = "dummyPath"
        val menuPrintService = MenuPrintService()

        //when
        val actual = menuPrintService.setConfigFilePath(path)

        //then
        actual shouldBe true
    }

    @Test
    fun `given blank string when setConfigFilePath then return false`() {
        //given
        val path = "     "
        val menuPrintService = MenuPrintService()

        //when
        val actual = menuPrintService.setConfigFilePath(path)

        //then
        actual shouldBe false
    }
}