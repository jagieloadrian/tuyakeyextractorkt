package service.extractor

import com.anjo.model.ExtractedDevice
import com.anjo.service.extractor.TuyaKeyExtractorService
import com.anjo.service.extractor.TuyaKeyJsonExtractor
import com.anjo.service.extractor.TuyaKeyXmlExtractor
import com.anjo.utils.Constants.DEFAULT_CSV_NAME
import com.anjo.utils.Constants.DEFAULT_TXT_NAME
import com.anjo.utils.Constants.EMPTY
import com.anjo.utils.Constants.INVALID_SEARCH_MESSAGE
import com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut
import constant.TestConstant.CURRENT_PATH
import constant.TestConstant.FILE_NOT_FOUND_TEST
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.mockk.every
import io.mockk.mockkConstructor
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.io.IOException


class TuyaKeyExtractorServiceTest {

    private lateinit var extractorService: TuyaKeyExtractorService
    private lateinit var keys: List<ExtractedDevice>

    @BeforeEach
    fun setup() {
        keys = listOf(
                ExtractedDevice("Device1", "localKey1", "deviceId1", "iconUrl1"),
                ExtractedDevice("Device2", "localKey2", "deviceId2", "iconUrl2"),
                ExtractedDevice("Device3", "localKey3", "deviceId3", "iconUrl3"),
                ExtractedDevice("Device4", "localKey4", "deviceId4", "iconUrl4")
        )
    }

    @Test
    fun `given list of extractorDevice and path when readRawJson then assert println`() {
        //given
        val path = "dummypath"
        val expected = keys.joinToString("\n") { it.toString() }
        mockkConstructor(TuyaKeyJsonExtractor::class)
        every { anyConstructed<TuyaKeyJsonExtractor>().readKeys(path) } returns keys
        extractorService = TuyaKeyExtractorService()

        //when
        val actual = tapSystemOut {
            extractorService.readRawJson(path)
        }

        //then
        actual shouldContain expected
    }

    @Test
    fun `given list of extractorDevice and path when readRawXml then assert println`() {
        //given
        val path = "dummypath"
        val expected = keys.joinToString("\n") { it.toString() }
        mockkConstructor(TuyaKeyXmlExtractor::class)
        every { anyConstructed<TuyaKeyXmlExtractor>().readKeys(path) } returns keys
        extractorService = TuyaKeyExtractorService()

        //when
        val actual = tapSystemOut {
            extractorService.readRawXml(path)
        }

        //then
        actual shouldContain expected
    }

    @Test
    fun `given list of extractorDevice and path when readJsonWithFormattedTable then assert println`() {
        //given
        val path = "dummypath"
        mockkConstructor(TuyaKeyJsonExtractor::class)
        every { anyConstructed<TuyaKeyJsonExtractor>().readKeys(path) } returns keys
        extractorService = TuyaKeyExtractorService()

        //when
        val actual = tapSystemOut {
            extractorService.readJsonWithFormattedTable(path)
        }

        //then
        keys.forEach { (deviceName, localKey, deviceId, iconUrl) ->
            actual shouldContain deviceName
            actual shouldContain localKey
            actual shouldContain deviceId
            actual shouldContain iconUrl
        }
    }

    @Test
    fun `given list of extractorDevice and path when readXmlWithFormattedTable then assert println`() {
        //given
        val path = "dummypath"
        mockkConstructor(TuyaKeyXmlExtractor::class)
        every { anyConstructed<TuyaKeyXmlExtractor>().readKeys(path) } returns keys
        extractorService = TuyaKeyExtractorService()

        //when
        val actual = tapSystemOut {
            extractorService.readXmlWithFormattedTable(path)
        }

        //then
        keys.forEach { (deviceName, localKey, deviceId, iconUrl) ->
            actual shouldContain deviceName
            actual shouldContain localKey
            actual shouldContain deviceId
            actual shouldContain iconUrl
        }
    }

    @Test
    fun `given empty string when searchByDeviceId then return error message`() {
        //given
        val path = "dummypath"
        val query = EMPTY
        extractorService = TuyaKeyExtractorService()

        //when
        val actual = tapSystemOut {
            extractorService.searchByDeviceId(path, query)
        }

        //then
        actual shouldContain INVALID_SEARCH_MESSAGE
    }

    @Test
    fun `given whitespace string when searchByDeviceId then return error message`() {
        //given
        val path = "dummypath"
        val query = "     "
        extractorService = TuyaKeyExtractorService()

        //when
        val actual = tapSystemOut {
            extractorService.searchByDeviceId(path, query)
        }

        //then
        actual shouldContain INVALID_SEARCH_MESSAGE
    }

    @Test
    fun `given deviceId when searchByDeviceId then return device`() {
        //given
        val path = "dummypath"
        val query = "deviceId1"
        val expectedDevice = ExtractedDevice("Device1", "localKey1", "deviceId1", "iconUrl1")
        mockkConstructor(TuyaKeyJsonExtractor::class)
        every { anyConstructed<TuyaKeyJsonExtractor>().readKeys(path) } returns keys
        extractorService = TuyaKeyExtractorService()

        //when
        val actual = tapSystemOut {
            extractorService.searchByDeviceId(path, query)
        }

        //then
        actual shouldContain expectedDevice.toString()
    }

    @Test
    fun `given deviceId when searchByDeviceId then return message not found`() {
        //given
        val path = "dummypath"
        val query = "dummySearchText"
        val expected = "Device with id $query not found in file with path: $path"
        mockkConstructor(TuyaKeyJsonExtractor::class)
        every { anyConstructed<TuyaKeyJsonExtractor>().readKeys(path) } returns keys
        extractorService = TuyaKeyExtractorService()

        //when
        val actual = tapSystemOut {
            extractorService.searchByDeviceId(path, query)
        }

        //then
        actual shouldContain expected
    }

    @Test
    fun `given devices when saveToCsv then assert created file`() {
        //given
        val path = "dummypath"
        mockkConstructor(TuyaKeyJsonExtractor::class)
        every { anyConstructed<TuyaKeyJsonExtractor>().readKeys(path) } returns keys
        extractorService = TuyaKeyExtractorService()

        //when
        extractorService.saveToCsv(path)

        //then
        assertIsExistAndClean(DEFAULT_CSV_NAME)
    }

    @Test
    fun `given devices when saveToTxt then assert created file`() {
        //given
        val path = "dummypath"
        mockkConstructor(TuyaKeyJsonExtractor::class)
        every { anyConstructed<TuyaKeyJsonExtractor>().readKeys(path) } returns keys
        extractorService = TuyaKeyExtractorService()

        //when
        extractorService.saveToTxt(path)

        //then
        assertIsExistAndClean(DEFAULT_TXT_NAME)
    }

    @Test
    fun `given mocked fileOutputStream with throwing error when saveToCsv then print exception`() {
        //given
        val path = "dummypath"
        mockkConstructor(TuyaKeyJsonExtractor::class)
        every { anyConstructed<TuyaKeyJsonExtractor>().readKeys(path) } throws IOException(FILE_NOT_FOUND_TEST)
        extractorService = TuyaKeyExtractorService()

        //when
        val actual = tapSystemOut {
            extractorService.saveToCsv(path)
        }

        //then
        actual shouldContain FILE_NOT_FOUND_TEST
    }

    @Test
    fun `given mocked fileOutputStream with throwing error when saveToTxt then print exception`() {
        //given
        val path = "dummypath"
        mockkConstructor(TuyaKeyJsonExtractor::class)
        every { anyConstructed<TuyaKeyJsonExtractor>().readKeys(path) } throws IOException(FILE_NOT_FOUND_TEST)
        extractorService = TuyaKeyExtractorService()

        //when
        val actual = tapSystemOut {
            extractorService.saveToTxt(path)
        }

        //then
        actual shouldContain FILE_NOT_FOUND_TEST
    }

    private fun assertIsExistAndClean(fileName: String) {
        val actual = File("$CURRENT_PATH/$fileName")
        actual.exists() shouldBe true
        actual.delete()
    }
}