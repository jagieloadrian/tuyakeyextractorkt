package service.extractor

import com.anjo.model.ExtractedDevice
import com.anjo.service.extractor.TuyaKeyXmlExtractor
import com.anjo.utils.Constants.FILE_NOT_EXIST_MESSAGE
import com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut
import constant.TestConstant.ICON_URL_REAL_TEST
import constant.TestConstant.TEST_FILE_XML_PATH
import constant.TestConstant.WRONG_TEST_FILE_XML_PATH
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import org.junit.jupiter.api.Test

class TuyaKeyXmlExtractorTest {

    @Test
    fun `given path when extractKeys then return expected keys`() {
        //given
        val expected = listOf(
                ExtractedDevice("TESTNAME4", "localkey4", "deviceId4", ICON_URL_REAL_TEST),
                ExtractedDevice("TESTNAME3", "localkey3", "deviceId3", ICON_URL_REAL_TEST),
                ExtractedDevice("TESTNAME2", "localKey2", "deviceId2", ICON_URL_REAL_TEST),
                ExtractedDevice("TESTNAME1", "localKey1", "deviceId1", ICON_URL_REAL_TEST)
        )
        val extractor = TuyaKeyXmlExtractor()

        //when
        val actual = extractor.readKeys(TEST_FILE_XML_PATH)

        //then
        actual shouldBe expected
    }

    @Test
    fun `given wrong path when extractKeys then fetch error message`() {
        //given
        val extractor = TuyaKeyXmlExtractor()

        //when
        val actual = tapSystemOut{
            extractor.readKeys(WRONG_TEST_FILE_XML_PATH)
        }

        //then
        actual shouldContain FILE_NOT_EXIST_MESSAGE
    }
}