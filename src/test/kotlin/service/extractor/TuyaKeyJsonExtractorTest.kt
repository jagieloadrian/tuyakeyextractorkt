package service.extractor

import com.anjo.model.ExtractedDevice
import com.anjo.service.extractor.TuyaKeyJsonExtractor
import com.anjo.utils.Constants
import com.github.stefanbirkner.systemlambda.SystemLambda
import constant.TestConstant
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import org.junit.jupiter.api.Test

class TuyaKeyJsonExtractorTest{

    @Test
    fun `given path when extractKeys then return expected keys`() {
        //given
        val expected = listOf(
                ExtractedDevice("TESTNAME4", "localkey4", "deviceId4", TestConstant.ICON_URL_REAL_TEST),
                ExtractedDevice("TESTNAME3", "localkey3", "deviceId3", TestConstant.ICON_URL_REAL_TEST),
                ExtractedDevice("TESTNAME2", "localKey2", "deviceId2", TestConstant.ICON_URL_REAL_TEST),
                ExtractedDevice("TESTNAME1", "localKey1", "deviceId1", TestConstant.ICON_URL_REAL_TEST)
        )
        val extractor = TuyaKeyJsonExtractor()

        //when
        val actual = extractor.readKeys(TestConstant.TEST_FILE_XML_PATH)

        //then
        actual shouldBe expected
    }

    @Test
    fun `given wrong path when extractKeys then fetch error message`() {
        //given
        val extractor = TuyaKeyJsonExtractor()

        //when
        val actual = SystemLambda.tapSystemOut {
            extractor.readKeys(TestConstant.WRONG_TEST_FILE_XML_PATH)
        }

        //then
        actual shouldContain Constants.FILE_NOT_EXIST_MESSAGE
    }
}