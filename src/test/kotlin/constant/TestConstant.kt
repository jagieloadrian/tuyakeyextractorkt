package constant

import com.anjo.utils.Constants
import com.anjo.utils.OptionConstants.ABOUT_COMMAND
import com.anjo.utils.OptionConstants.EIGHTH_OPTION
import com.anjo.utils.OptionConstants.EMPTY_COMMAND
import com.anjo.utils.OptionConstants.EXTRAS_OPTION
import com.anjo.utils.OptionConstants.FIFTH_OPTION
import com.anjo.utils.OptionConstants.FOURTH_OPTION
import com.anjo.utils.OptionConstants.GITLAB_COMMAND
import com.anjo.utils.OptionConstants.HOW_TO_COMMAND
import com.anjo.utils.OptionConstants.LINKS_COMMAND
import com.anjo.utils.OptionConstants.MAIN_MENU_COMMAND
import com.anjo.utils.OptionConstants.PRESS_M_COMMAND
import com.anjo.utils.OptionConstants.QUIT_OPTION
import com.anjo.utils.OptionConstants.RELATED_VIDEOS_COMMAND
import com.anjo.utils.OptionConstants.SECOND_OPTION
import com.anjo.utils.OptionConstants.SET_CONFIG_PATH_OPTION
import com.anjo.utils.OptionConstants.SEVENTH_OPTION
import com.anjo.utils.OptionConstants.SIXTH_OPTION
import com.anjo.utils.OptionConstants.THIRD_OPTION
import com.anjo.utils.OptionConstants.VIDEO_COMMAND

object TestConstant {
    val CURRENT_PATH = System.getProperty("user.dir").toString()
    val TEST_FILE_XML_PATH = "$CURRENT_PATH/src/test/resources/test-preferences.xml"
    const val WRONG_TEST_FILE_XML_PATH = "wrongPathGirl!"
    const val DEVICE_NAME_TEST = "deviceName"
    const val LOCAL_KEY_TEST = "localKey"
    const val DEVICE_ID_TEST = "deviceId"
    const val ICON_URL_TEST = "iconUrl"
    const val FILE_NOT_FOUND_TEST = "File not found in test"
    const val ICON_URL_REAL_TEST = "https://tsh.io/wp-content/uploads/2021/10/no-integration-tests-meme-1.png"
    val MENU_OPTION_EXPECTED = listOf(SET_CONFIG_PATH_OPTION(Constants.NOT_SET), SECOND_OPTION, THIRD_OPTION,
            FOURTH_OPTION, FIFTH_OPTION, SIXTH_OPTION, SEVENTH_OPTION, EIGHTH_OPTION, EXTRAS_OPTION, QUIT_OPTION)

    val ALL_COMMANDS_EXPECTED = listOf(SET_CONFIG_PATH_OPTION(Constants.NOT_SET), SECOND_OPTION, THIRD_OPTION,
            FOURTH_OPTION, FIFTH_OPTION, SIXTH_OPTION, SEVENTH_OPTION, EIGHTH_OPTION, EXTRAS_OPTION, QUIT_OPTION,
            EMPTY_COMMAND, MAIN_MENU_COMMAND, ABOUT_COMMAND, LINKS_COMMAND, GITLAB_COMMAND, RELATED_VIDEOS_COMMAND,
            VIDEO_COMMAND, HOW_TO_COMMAND, PRESS_M_COMMAND)

    private const val OPTION_1 = "1"
    private const val OPTION_2 = "2"
    private const val OPTION_3 = "3"
    private const val OPTION_4 = "4"
    private const val OPTION_5 = "5"
    private const val OPTION_6 = "6"
    private const val OPTION_7 = "7"
    private const val OPTION_8 = "8"
    private const val OPTION_EXTRAS = "e"
    private const val OPTION_ABOUT_TOOL = "a"
    private const val OPTION_GITLAB = "g"
    private const val OPTION_HOW_TO = "h"
    private const val OPTION_VIDEO = "v"
    private const val OPTION_MAIN_MENU = "m"
    private const val OPTION_QUIT = "q"
    private const val EMPTY_SPACE = " "
    private const val DEVICE_ID_1 = "deviceId1"
    val TESTED_COMMAND_OPTIONS =
        arrayOf(OPTION_1, TEST_FILE_XML_PATH, EMPTY_SPACE, OPTION_2, EMPTY_SPACE, OPTION_3, EMPTY_SPACE, OPTION_4,
                EMPTY_SPACE, OPTION_5, EMPTY_SPACE, OPTION_6, DEVICE_ID_1, EMPTY_SPACE, OPTION_7, EMPTY_SPACE, OPTION_8,
                EMPTY_SPACE, OPTION_EXTRAS, EMPTY_SPACE, OPTION_ABOUT_TOOL, EMPTY_SPACE, OPTION_EXTRAS, EMPTY_SPACE,
                OPTION_GITLAB, EMPTY_SPACE, OPTION_EXTRAS, EMPTY_SPACE, OPTION_VIDEO, EMPTY_SPACE, OPTION_EXTRAS,
                EMPTY_SPACE, OPTION_HOW_TO, EMPTY_SPACE, OPTION_EXTRAS, EMPTY_SPACE, OPTION_MAIN_MENU, EMPTY_SPACE,
                OPTION_QUIT)
}