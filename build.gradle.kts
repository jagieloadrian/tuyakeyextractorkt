import org.jetbrains.kotlin.gradle.plugin.mpp.pm20.util.archivesName

plugins {
    application
    kotlin("jvm") version "1.9.23"
    id("edu.sc.seis.launch4j") version "3.0.5"
}

group = "com.anjo"

repositories {
    mavenCentral()
}

application {
    mainClass.set("MainKt") // The main class of the application
    archivesName.set(rootProject.name)
}

dependencies {
    implementation("org.json:json:20240303")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.16.1")
    implementation("de.vandermeer:asciitable:0.3.2")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("io.mockk:mockk:1.13.8")
    testImplementation("io.kotest:kotest-assertions-core:5.5.5")
    testImplementation("com.github.stefanbirkner:system-lambda:1.2.1")
}

kotlin {
    jvmToolchain(17)
}

//It has to be under dependencies and kotlin block
tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "com.anjo.MainKt"
    }
    configurations["compileClasspath"].forEach { file: File ->
        from(zipTree(file.absoluteFile))
    }
    duplicatesStrategy = DuplicatesStrategy.INCLUDE
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<edu.sc.seis.launch4j.tasks.DefaultLaunch4jTask> {
    mustRunAfter("build")
    outfile.set("${rootProject.name}.exe")
    mainClassName.set("MainKt")
    icon.set("$projectDir/src/main/resources/favicon.ico")
    productName.set(rootProject.name)
    outputDir.set("libs")
}
