![icon-512.png](src%2Fmain%2Fresources%2Ficon-512.png)
# Tuya Key Extractor Kt

Kotlin's application for extracting local key from android xml preferences file, which can be obtained as in this video:
[YT](https://www.youtube.com/watch?v=YKvGYXw-_cE)

## Table of Contents
* [General Info](#general-information)
* [Download](#download)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)

## General Information

It's application influenced by this [movie](https://www.youtube.com/watch?v=YKvGYXw-_cE) I decided to write kotlin version
to have possibility run on other systems than Windows. It's also have executable file to run on Windows system.

## Technologies Used
- Kotlin - version 1.9
- Jackson
- Launch4J

## Features
List the ready features here:
- read file from path
- list all devices
- save to CSV or TXT file
- search by deviceId

## Setup
Java 17, newest version Intellij/Android Studio (it can be build-in in IDE) to run locally.

## Usage
- Windows -> download file with extension `*.exe` and run (i.e. double click)
- Linux/MacOs -> download file with extension `*.jar` and run by command
  `java -jar TuyaKeyExtractorKt.jar`

## Download
- [TuyaKeyExtractorKt-JAR](https://gitlab.com/api/v4/projects/55629469/jobs/artifacts/main/raw/build/libs/TuyaKeyExtractorKt.jar?job=build)
- [TuyaKeyExtractorKt-EXE](https://gitlab.com/api/v4/projects/55629469/jobs/artifacts/main/raw/build/libs/TuyaKeyExtractorKt.exe?job=build)

## Project Status
Project is: finished

## Room for Improvement
- Add more devices to handle than moisture hygrometer

## Acknowledgements
- This project was inspired by Mark Watt solution
- Not tested on MacOs,
- Tested only on Ubuntu 22 and Windows 11

## Contact
Created by [@jagieloadrian](https://gitlab.com/jagieloadrian) - feel free to contact me!

